require "yaml"
require 'httparty'
require 'open-uri'

class TpmApi < Rails::Application
  include HTTParty    
  cattr_accessor :url, :username, :pwd, :label_private_key, :project_id

  # load default setting
  def self.init
    cnf = YAML.load(ERB.new(File.read("#{Rails.root}/config/tpm_api.yml")).result)[Rails.env]
    self.url = cnf["url"]  
    self.username = cnf["username"]
    self.pwd = cnf["pwd"]
    self.label_private_key = cnf["label_private_key"] || "Private Key"

    # find or create default project by name
    project_name = cnf["project_name"] || "Tapptic's internal certificate renewal tool"
    project_tags = cnf["project_tags"] || "project"
    project = TpmApi.get("/projects/search/#{CGI.escape(project_name)}.json").try(:first)

    if project.present?
      self.project_id = project["id"]
    else
      self.project_id = TpmApi.post("/projects.json", {name: project_name, tags: project_tags})["id"]
    end
  end

  # get method api
  # url ex: "/passwords.json" => get list passwords
  # options is hash data query.
  def self.get(url, options = {})
    HTTParty.get("#{self.url}#{url}", :headers => {'Content-Type' =>'application/json; charset=utf-8'}, :basic_auth => {:username => self.username, :password => self.pwd} )
  end

  # post method api
  # url ex: "/projects.json" => create passwords
  # options is hash data query. ex: TpmApi.post("/projects", {name: 'abc', tags: 'test,tool'})  
  def self.post(url, options = {})
    HTTParty.post("#{self.url}#{url}", :body => options.to_json, :headers => {'Content-Type' =>'application/json; charset=utf-8'}, :basic_auth => {:username => self.username, :password => self.pwd})
  end

  # put method api
  # url ex: "/passwords/ID.json" => update password with id=ID
  # options is hash data query. ex: TpmApi.put("/passwords/1.json", {name: 'change name password'})  
  def self.put(url, options = {})
    HTTParty.put("#{self.url}#{url}", :body => options.to_json, :headers => {'Content-Type' =>'application/json; charset=utf-8'}, :basic_auth => {:username => self.username, :password => self.pwd})
  end

  # delete method api
  # url ex: "/projects/ID.json" => delete project with id=ID
  def self.delete(url)
    HTTParty.delete("#{self.url}#{url}", :body => options.to_json, :headers => {'Content-Type' =>'application/json; charset=utf-8'}, :basic_auth => {:username => self.username, :password => self.pwd})
  end

  # upload private key will call 2 api:
  # 1- create password with pwd=passphrase, name=name, custom_field=content
  # 2- update label_custom_field to math with config in file tpm_api.yml [label_private_key]
  def self.upload_private_key(name, passphrase="", content)    
    new_pwd = TpmApi.post("/passwords.json", {name: name, password: passphrase, project_id: self.project_id, :custom_data1 => content})
    TpmApi.put("/passwords/#{new_pwd['id']}/custom_fields.json", {:custom_label1=>self.label_private_key, :custom_type1 =>"text"})
    new_pwd
  end

end

TpmApi.init()