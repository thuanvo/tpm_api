Gem::Specification.new do |s|
  s.name        = 'tpm_api'
  s.version     = '0.0.1'
  s.date        = '2015-04-15'
  s.summary     = "Team password management API!"
  s.description = "Wrap Team password management API"
  s.authors     = ["author"]
  s.email       = 'author@example.com'
  s.files       = ["lib/tpm_api.rb"]
  s.homepage    = 'http://rubygems.org/gems/tpm_api'
  s.license       = 'MIT'

  s.add_dependency "httparty"

end